# Barèmes IPP

Original IPP's tax and benefit tables.

**THIS PROJECT HAS MIGRATED TO https://gitlab.com/ipp/partage-public-ipp/baremes-ipp/baremes-ipp-edit.**

IPP is the [Institut des politiques publiques](http://www.ipp.eu/en/)

Published tax and benefit tables:
- English: http://www.ipp.eu/en/tools/ipp-tax-and-benefit-tables/
- French: http://www.ipp.eu/fr/outils/baremes-ipp/

## Data License

Licence ouverte / Open Licence <http://www.etalab.gouv.fr/licence-ouverte-open-licence>

## Contribute to IPP's tax and benefit tables

If you wish to contribute, you may find the following documentation helpful (in french)

* [Edition guide](doc/guide_edition.md)
* [Guide to read the legislation](doc/guide_legislation.md)
* [Updates census](doc/recensement_actualisations.md)
* [Some history about the IPP's tax and benefit tables](doc/historique.md)
* [When to update IPP's tables ? A calendar of tax and benefit legislation changes](doc/calendrier.md)

 Les contributeurs sont invités à consulter la documentation détaillés dans les sections suivantes:

* [Guide d'édition des barèmes IPP](doc/guide_edition.md)
* [Un petit guide pour se répérer dans la législation](doc/guide_legislation.md)
* [Recensement des actualisations des barèmes IPP](doc/recensement_actualisations.md)
* [L'évolution des barèmes IPP](doc/historique.md)
* [Calendrier des publications des paramètres législatifs](doc/calendrier.md)
