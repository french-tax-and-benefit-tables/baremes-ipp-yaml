# Guide d'édition des barèmes IPP

Ce document vous guide étape par étape pour l'édition des barèmes IPP.

- [Étape 1 : Retrouver le fichier YAML du paramètre à modifier](#1-retrouver-le-fichier-YAML-du-paramètre-à-modifier)
- [Étape 2 : Modifier un paramètre](#2-modifier-le-paramètre)
- [Étape 3 : Enregistrer la modification (Commit)](#3-enregistrer-la-modification-commit)
- [Étape 4 : Ajouter la modification aux barèmes IPP publiés (Merge Request)](#4-ajouter-la-modification-aux-barèmes-IPP-publiés-merge-request)
- [Étape 5 : Noter mon travail dans le tableau recensant les actualisations des barèmes](#5-noter-mon-travail-dans-le-tableau-recensant-les actualisations-des-barèmes)
- [FAQ](#foire-aux-questions)


## Étape 1. Retrouver le fichier YAML du paramètre à modifier

- Les barèmes IPP sont visibles par tous [sur ce site](https://baremes-ipp.netlify.com/).
En vous rendant sur ce site, vous pouvez naviguer dans l'arbre des paramètres législatifs jusqu'à atteindre le paramètre recherché.
Pour voir le site généré par votre branche : `https://nom_de_la_branche--baremes-ipp.netlify.com/`

- Sur la page du paramètre recherché, cliquez sur le lien <kbd>Edit</kbd> situé sous le paramètre que vous souhaitez modifier.
Vous êtes alors automatiquement redirigé vers le [dépôt (_repository_) Git "baremes-ipp-yaml"](https://framagit.org/french-tax-and-benefit-tables/baremes-ipp-yaml)
au niveau du fichier YAML contenant le paramètre idoine.

Le format [YAML](https://fr.wikipedia.org/wiki/YAML) est le format retenu pour les paramètres législatifs d'[OpenFisca](https://openfisca.org/doc/coding-the-legislation/legislation_parameters.html).
Il permet de représenter de façon lisible des informations élaborées comme une combinaison de listes et de dictionnaires imbriqués.
Il est nécessaire de bien respecter:
- l'indentation introduisant les sous-structures
- les tirets `-` marquant des listes
- les deux points séparant les clés des valeurs dans un dictionnaire

## Étape 2. Modifier le paramètre

Avant de modifier les barèmes IPP pour la première fois,
il convient de lire [**les règles d'édition**](./guide_legislation.md)
des paramètres législatifs.

Pour modifier le fichier YAML, cliquez sur le bouton <kbd>Edit</kbd> situé dans le coin supérieur droit de la page.

### Option 1 : Modifier/corriger un paramètre existant

#### Format des dates

  - Les dates sont omniprésentes dans les fichiers contenant les paramètres IPP. Le format retenu est, sauf cas particulier (mention explicit dans un texte cité, etc), est YYY-MM-AA (exemple: `2011-08-01`)
  - Si plusieurs dates doivent êtres mentionnées, elles sont séparées par un `;` (exemple: `2012-01-01, 2011-12-31`)


#### Structure

  - Chaque fichier YAML peut regrouper un ou plusieurs paramètres en plus d'une description et de méta-données

    - Le champ `description` servira de titre à la table ou à la colonne rassemblant les valeurs des paramètres qu'elle contient (les lignes étant des dates).

    - Le champ `metadata` peut contenir les champs suivants qui viendront s'ajouter aux colonnes :

      - `order`: indique l'ordre dans lequel sont rangés les paramètres

      - `reference`: les références législatives classées par date
      - `official_journal_date`: les dates de parution au Journal officiel classées par date
      - `note`: les éventuelles remarques sur le changement de législation, classées par date

  - Chaque paramètre dispose également d'une `description` et de méta-données (`metadata`)

    - Le champ `description` servira de nom au paramètre
    - Le champ `metadata` décrira certaines caractéristiques des paramètres qu'il s'agira de respecter lors de l'édition de ses valeurs

#### Marche à suivre

  - Dans le champ `values` du paramètre, ajoutez la nouvelle valeur du paramètre et la date de début correspondante (ou corrigez la valeur, le cas échéant).

  - Dans le champ `reference` de `metadata`, ajoutez la référence législative justifiant votre changement ainsi que (si possible) le lien URL vers cette référence sur le site Legifrance. Il est préférable d'ajouter un lien ELI ou Alias (disponible en haut de la page Legifrance d'un texte donné) plutôt que le lien qui s'affiche sur la barre de navigation.
  - Dans le champ `official_journal_date` de `metadata`, ajoutez la date de parution au Journal officiel de la référence.
  - Cas particulier : si un paramètre disparaît (ex : une allocation supprimée), il faut rajouter une valeur égale à `null` à l'année de sa disparition. Pour les années suivantes il n'y a plus besoin de le modifier.

- Exemple
  Je souhaite mettre à jour la valeur du plafond de la Sécurité Sociale pour 2019.
  Pour l'instant, les barèmes IPP s'arrêtent en 2018. Je rajoute sa nouvelle valeur qui prend effet à partir du 1er janvier 2019 et la référence législative (ainsi que le lien URL vers cette référence).

  *NB : cet exemple est entièrement fictif.*

  ```diff
   plafond_securite_sociale_annuel:
   description: Plafond de la Sécurité sociale (annuel)
   values:
  +  2019-01-01:
  +    value: 50000.0
     2018-01-01:
      value: 39732.0
  ```
  ```diff
   metadata:
     order:
       - plafond_securite_sociale_mensuel
       - plafond_securite_sociale_annuel
     reference:
  +   2019-01-01:
  +     title: Loi 2019-1479, art.3
  +     href: https://www.legifrance.gouv.fr/eli/loi/2019/12/28/CPAX1925229L/jo/texte
      2018-01-01: Arrêté du 05/12/2017
      2017-01-01: Arrêté du 05/12/2016
      [...]
     official_journal_date:
  +   2019-01-01: 2018-12-13
      2018-01-01: 2017-12-09
  ```

#### Option 2 : Ajouter un nouveau paramètre dans un fichier YAML

#### Marche à suivre
  - Ajoutez le nom de votre paramètre : en tout attaché, sans majuscules, sans espaces (utillisez des tirets du bas `_` comme séparateur si nécessaire)
  - Ajoutez un champ `description`
  - Ajoutez un champ `values`
  - Ajoutez un champ `metadata` avec deux sous-champs : `ipp_csv_id` et `unit`.
     - Les unités possibles pour les paramètres sont des nombres sans unités (``/1``) comme des taux ou des nombres d'enfants, des valeurs monétaires (``currency-EUR`` ou ``currency-FRF``) ou des années (``year``)
     - Le champ `ipp_csv_id` correspond simplement au nom du paramètre pour export vers la version Stata de TAXIPP. Il convient donc de lui donner un nom compréhensible mais court (moins de 32 caractères car c'est la limite dans Stata).
  - Identifiez visuellement la place de votre nouveau paramètre dans le barème (c'est important pour la visualisation sur le site de l'IPP)
    Il y a souvent plusieurs paramètres par fichier YAML, et il peut aussi y avoir plusieurs sous-sections par fichiers YAML. Il faut donc choisir la manière dont ils s'affichent.
  - Dans la section déjà existante `order` de `metadata` du fichier YAML, ajoutez le nom de votre paramètre (insérez-le à l'endroit choisi)

 - Exemple
   Je souhaite ajouter le paramètre de taux de dégressivité de la prime d'activité (PPA).

   *NB : cet exemple est fictif, ce paramètre existe déjà.*

    ```diff
    + majoration_ressources_revenus_activite:
    +   description: Majoration des ressources sur les revenus d'activité
    +   values:
    +     2016-01-01:
    +       value: 0.62
    +   metadata:
    +     ipp_csv_id: pa_maj_revenu
    +     unit: /1
    ```
    ```diff
    metadata:
    order:
     - majoration_montant_maximal_en_base_rsa
     - majoration_isolement_en_base_rsa
    + - majoration_ressources_revenus_activite
     - bonification
     - montant_minimum_verse
    reference:
    ```

### Option 3 : Créer un nouveau fichier YAML

- Sur le [dépôt (_repository_) Git "baremes-ipp-yaml"](https://framagit.org/french-tax-and-benefit-tables/baremes-ipp-yaml), placez-vous dans le dossier correspondant à la partie dans laquelle le nouveau barème doit se situer.
- Cliquez sur le bouton <kbd>**+**</kbd> en haut, puis sur <kbd>New file</kbd>.
- Donnez un nom au nouveau fichier dans le champ correspondant, en spécifiant bien l'extension `.yaml`.
- Rédigez le fichier, en respectant les règles de format décrites à [l'option 1](#option-1-modifier-corriger-un-paramètre-existant) ; enregistrez la modification (voir [ci-dessous](#etape-3-enregistrer-la-modification-commit)).
- Modifiez le fichier `index.yaml` situé dans le même dossier, en ajoutant à l'index le nouveau barème que vous avez créé. Enregistrez la modification (voir ci-après).


## Étape 3. Enregistrer la modification (COMMIT)

Une fois que vous avez modifié le barème dans l'éditeur en ligne,
il est nécessaire de *"commiter"* ce changement sinon il ne sera pas enregistré.

- Entrez un message de commit dans la case <kbd>Commit message</kbd> en bas de votre éditeur (cf .[les règles d'écriture d'un message de commit utile](https://chris.beams.io/posts/git-commit/)).
- Entrez le nom de la branche sur laquelle vous souhaitez effectuer le commit, dans la case <kbd>Target branch</kbd> (donnez lui un nom qui a un sens !)
- Cochez la case <kbd>Start a new merge request with these changes</kbd>

Pour les débutants : [quelque rappels sur Git et le version control](https://framagit.org/ipp/ipp-survival-gitbook/blob/master/git.md)

Pour rappel, la visualisation du site généré par votre branche se fait sur : `https://nom_de_la_branche--baremes-ipp.netlify.com/`

## Étape 4. Ajouter la modification aux barèmes IPP publiés (MERGE REQUEST)

Si vous avez demandé l'ouverture d'une Merge Request, vous êtes automatiquement redirigé vers la page de cette Merge Request.
Pour rappel, une Merge Request est une demande de fusion des modifications que vous avez effectuées sur votre branche dans une autre branche (généralement la branche principale `master`). La page de la Merge Request permet d'échanger sur les changements opérés en vue d'une validation avant fusion définitive (merge).
Ici la branche "source" est votre branche personnelle sur laquelle vous avez modifié les barèmes et la branche "target" est la branche `master`.

- Donnez un titre à votre Merge Request : ce titre doit décrire en 1 ligne l'ensemble des changements effectués (soyez concis)
- Choisissez un template pour votre Merge Request (menu déroulant `Choose a template`) selon l'opération effectuée (correction de barèmes, mise à jour ...)
- Dans la description, décrivez plus en détail chaque changement, avec éventuellement des liens vers vos références etc.
Vous pouvez retrouver tout en bas de la page, la liste des commits que vous avez faits sur votre branche. En cliquant dessus, vous pouvez visualiser les changements introduits.
- Finalisez votre Merge Request en cliquant sur <kbd>Submit merge request</kbd>. Vous pourrez toujours la modifier par la suite en cliquant sur le bouton <kbd>Edit</kbd>.
- Une fois votre Merge Request ouverte, il vous appartient de la faire aboutir, c'est-à-dire de faire en sorte que votre branche soit mergée. Pour cela, et lorsque vous avez fini de faire vos changements, demandez une `review` à un autre membre. Il vous suffit de désigner un `"Assignee"` (cf. barre de droite en haut, cliquez sur `Edit`) qui se chargera de faire une revue ("review") de votre travail et qui acceptera ou non la Merge Request.

## Étape 5. Noter mon travail dans le tableau recensant les actualisations des barèmes

Une fois votre merge request validée, c'est à vous de la merger. Avant cela, Il est important que chacun puisse savoir, lorsqu'il souhaite utiliser ou actualiser un barème, dans quelle mesure ce barème est à jour. Regarder l'historique des merges requests peut être lourd. De plus, il arrive (rarement ceci dit) que l'on souhaite actualiser un barème mais qu'aucun changement ne soit à faire du fait que la législation n'ait pas été modifiée. Aucune merge request n'est recensée pour ce cas de figure. Ainsi, une fois que votre travail est mergé, vous devez le rencenser dans le tableau figurant dans [la page dédiée à cet effet](recensement_actualisations.md). Si votre actualisation fait l'objet d'une merge request, il faut, pour une meilleure lisibilité, **faire la modification de ce tableau dans votre merge request** (le commit de modification du tableau doit figurer dans votre branche).

## Foire aux questions

- [Guide d'édition des barèmes IPP](#guide-dédition-des-barèmes-ipp)
  - [Étape 1. Retrouver le fichier YAML du paramètre à modifier](#étape-1-retrouver-le-fichier-yaml-du-paramètre-à-modifier)
  - [Étape 2. Modifier le paramètre](#étape-2-modifier-le-paramètre)
    - [Option 1 : Modifier/corriger un paramètre existant](#option-1--modifiercorriger-un-paramètre-existant)
      - [Format des dates](#format-des-dates)
      - [Structure](#structure)
      - [Marche à suivre](#marche-à-suivre)
      - [Option 2 : Ajouter un nouveau paramètre dans un fichier YAML](#option-2--ajouter-un-nouveau-paramètre-dans-un-fichier-yaml)
      - [Marche à suivre](#marche-à-suivre-1)
    - [Option 3 : Créer un nouveau fichier YAML](#option-3--créer-un-nouveau-fichier-yaml)
  - [Étape 3. Enregistrer la modification (COMMIT)](#étape-3-enregistrer-la-modification-commit)
  - [Étape 4. Ajouter la modification aux barèmes IPP publiés (MERGE REQUEST)](#étape-4-ajouter-la-modification-aux-barèmes-ipp-publiés-merge-request)
  - [Étape 5. Noter mon travail dans le tableau recensant les actualisations des barèmes](#étape-5-noter-mon-travail-dans-le-tableau-recensant-les-actualisations-des-barèmes)
  - [Foire aux questions](#foire-aux-questions)
    - [Je ne peux pas éditer le fichier YAML sur le dépôt Git `baremes-ipp-yaml`](#je-ne-peux-pas-éditer-le-fichier-yaml-sur-le-dépôt-git-baremes-ipp-yaml)
    - [Je souhaite faire plusieurs modifications groupées / J'ai oublié d'ajouter quelque chose](#je-souhaite-faire-plusieurs-modifications-groupées--jai-oublié-dajouter-quelque-chose)
    - [Je souhaite annuler mes modifications](#je-souhaite-annuler-mes-modifications)
    - [Je souhaite faire une modification, mais je n'ai pas la référence législative](#je-souhaite-faire-une-modification-mais-je-nai-pas-la-référence-législative)
    - [My pipeline has failed](#my-pipeline-has-failed)

### Je ne peux pas éditer le fichier YAML sur le dépôt Git `baremes-ipp-yaml`

Afin de pouvoir éditer, il faut être membre du projet *baremes-ipp-yaml* et avoir un
statut au moins équivalent à celui de "Developper" pour avoir des droits d'édition.
Demandez à être rajouté par [un des membres autorisé ("Owner")](https://framagit.org/french-tax-and-benefit-tables/baremes-ipp-yaml/project_members) à le faire.

### Je souhaite faire plusieurs modifications groupées / J'ai oublié d'ajouter quelque chose

Il est possible de faire plusieurs petites modifications des barèmes (commits) et de les grouper ensemble dans une seule Merge Request.
Dans ce cas, il vous faut changer de branche. Pour cela rendez vous dans `Repository` (cf. barre de navigation à gauche dans Framagit). Vous vous trouvez normalement dans la branche Master, c'est indiqué en haut de la page. Changez de branche grâce au menu déroulant et choisissez votre nouvelle branche.
Vous pouvez effectuer désormais plusieurs commits sur la même branche (c'est-à-dire répéter les étapes 2 et 3). N'oubliez pas de ne **pas** cocher <kbd>Start a new Merge Request</kbd> pour que tous vos commits soient regroupés dans la même merge request.

Si vous n'aviez pas encore ouvert de Merge Request lors du premier commit, une fois que vous avez fini, vous pouvez vous rendre sur la page ["branches"](https://framagit.org/french-tax-and-benefit-tables/baremes-ipp-yaml/branches)
pour retrouver votre branche et cliquer sur le bouton <kbd>Merge Request</kbd>.

Même une fois la Merge Request ouverte, il est possible de continuer à faire des commits sur votre branche si vous avez oublié de modifier quelque chose.

NB : Une Merge Request doit, autant que possible, introduire un groupe de changements cohérents.

- Exemple 1 :

  Le gouvernement annonce une revalorisation exceptionnelle des prestations sociales de 10%.
  Je modifie le barème du montant de base du RSA (1 commit), puis de la prime d'activité (1 commit) puis des APL (1 commit)
  Je fais donc une seule Merge Request avec tous ces commits.

- Exemple 2 :

  Le PLF 2019 introduit une revalorisation de l'ASPA et une augmentation de la taxation indirecte.
  Je fais donc deux Merge Request distinctes.

### Je souhaite annuler mes modifications

- Si vous vous rendez compte de votre erreur avant d'avoir fait votre commit, pas de soucis, le changement n'a pas été pris en compte.
- Si vous vous rendez compte de votre erreur après avoir fait le commit mais avant de merger, pas de soucis, il suffit de faire un nouveau commit qui corrige votre erreur.
- Si vous vous rendez compte de votre erreur après que vos changement aient été mergés (c'est-à-dire qu'ils ont été incorporés au barèmes IPP officiels, dans la branche `master`)
  alors il faut créer au plus vite une nouvelle branche avec un commit qui corrige votre erreur et créer une nouvelle merge request.

### Je souhaite faire une modification, mais je n'ai pas la référence législative

Si vous n'avez pas ou ne retrouvez pas, la référence législative de votre changement, il est préférable de ne pas ouvrir de Merge Request tout de suite.
Par contre, il est possible d'[ouvrir un ticket ("issue")](https://framagit.org/french-tax-and-benefit-tables/baremes-ipp-yaml/issues) et d'expliquer votre problème de manière détaillée pour que d'autres contributeurs puissent vous aider.

### My pipeline has failed

Une première source d'erreur assez fréquente correspond à une situation où vous un onglet avec le site web des barèmes IPP ouvert (la version générée par votre branche).
Lorsque vous ouvrez une Merge Request ou commitez sur votre branche, cela lance le pipeline qui essaye notamment de reconstruire le site avec vos modifications.
Si celui-ci est ouvert, il n'y arrive pas, d'où le fail. Commencez donc par vous assurer que le site n'est pas ouvert.

Une deuxième source d'erreur fréquente est l'utilisation des `:` dans le texte, par exemple en Notes, en Documentation ou en Sources. Cet usage est possible *uniquement si ce texte est entouré de guillemets*. Sinon, la lecture du yaml sera perturbée car le `:` a un usage spécifique dans l'arborescence du barème. Par exemple, il faut écrire

`"Valeur T2 provient du rapport du Sénat sur le PLF 2020 : https://www.senat.fr/rap/l19-140-324/l19-140-3244.html"`

et non

`Valeur T2 provient du rapport du Sénat sur le PLF 2020 : https://www.senat.fr/rap/l19-140-324/l19-140-3244.html`
