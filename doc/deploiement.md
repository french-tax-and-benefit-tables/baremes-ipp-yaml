# Déploiement sur ipp.eu

Les mises à jour de la branche `master` de ce dépôt sont régulièrement et automatiquement déployées sur https://www.ipp.eu/baremes-ipp/.

Un fichier ZIP contenant les pages partielles à intégrer sur le site est publié sur la branche `ipp.eu-wp-zip` par GitLab CI (voir [configuration](../.gitlab-ci.yml)) à chaque mise à jour de `master`.

Le rendu des pages partielles de `ipp.eu-wp-zip` sont génèrées par l'application [baremes-ipp-views](https://framagit.org/french-tax-and-benefit-tables/baremes-ipp-views/).

Le traitement de ce fichier par WordPress est décrite dans une [documentation spécifique](https://bitbucket.org/juliabr/ipp.eu/src/master/wp-content/themes/ipp/README_BAREMES.md) sur le dépôt du site.

Afin de modifier le `CSS` de ipp.eu, il faut modifier le fichier `wp-content/themes/ipp/src/scss/parts/_baremes.scss` sur le [dépot du site](https://bitbucket.org/juliabr/ipp.eu/src/master/wp-content/themes/ipp/src/scss/parts/_baremes.scss).

Afin de modifier le `JS` de ipp.eu, il faut modifier le fichier `wp-content/themes/ipp/assets/js/baremepage.js` sur le [dépot du site](https://bitbucket.org/juliabr/ipp.eu/src/master/wp-content/themes/ipp/assets/js/baremepage.js).


Sur le serveur hébergeant le site de l'IPP, le script qui déploie les barèmes est `~/www/wp-content/themes/ipp/deploy-baremes.php`
Le zip est mis dans `~/www/wp-content/themes/ipp/baremes.zip`
Les barèmes sont dézippés dans `~/www/wp-content/themes/ipp/baremes/`
Si un dézip échoue une nouvelle fois, il suffit de recopier à la main le contenu du zip des barèmes dans ce répertoire.
