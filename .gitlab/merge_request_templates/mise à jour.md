# Mise à jour d'un paramètre législatif

## Champ de la mise à jour 

(Merci d'effacer les blocs inutiles)

- chomage
/label chomage
/assign @benjello  	

- fiscalité des entreprises
/label entreprises
/assign @aguillouzouic

- impot-revenu 
/label impot-revenu
/assign @ClaireLeroyIPP 

- marche-travail 
/label marche-travail
/assign @audreyrain	

- prelevements-sociaux 
/label prelevements-sociaux
/assign @SophieIPP

- prestations-sociales
/label prestations-sociales
/assign @MarionIPP

- retraite 
/label retraite
/assign @ChloeLallemand

- tarifs-energie 
/label tarifs-energie
/assign @pdp

- taxation-capital 
/label taxation-capital
/assign @bfabre

- taxation-indirecte 
/label taxation-indirecte
/assign @benjello

## Description de la mise à jour

Décrire brièvement le mise à jour réalisée

----

## Quelques conseils à prendre en compte

### Vérifications à effectuer par le contributeur

- [ ] Décrire la mise à jour et son champ
- [ ] Renseigner la référence législative
- [ ] Renseigner la date de parution au JO
- [ ] Renseigner les changements portés par la merge request dans [ce tableau](https://framagit.org/french-tax-and-benefit-tables/baremes-ipp-yaml/-/blob/master/doc/recensement_actualisations.md) (Voir documentation, partie ["Etape 5" : Noter mon travail dans le tableau recensant les actualisations des barèmes](https://framagit.org/french-tax-and-benefit-tables/baremes-ipp-yaml/-/blob/master/doc/guide_edition.md#5-noter-mon-travail-dans-le-tableau-recensant-les%20actualisations-des-bar%C3%A8mes))
- [ ] Vérifier que les pipelines passent bien. En cas d'échec, et si le message d'échec est peu clair, s'assurer que les textes (notes, documentation...) incluant des `:` sont mis entre guillemets. ([Voir documentation, partie "My pipeline has failed"](https://framagit.org/french-tax-and-benefit-tables/baremes-ipp-yaml/-/blob/modif-template/doc/guide_edition.md))
- [ ] Ne pas oublier de vérifier que le site web est fonctionnel en cliquant [ici](https://nom_de_la_branche--baremes-ipp.netlify.com/) et en incluant le nom de votre branche dans l'URL s'affichant sur le navigateur.


### Vérifications à effectuer par le relecteur

- [ ] Vérifier que la date de parution au JO est renseignée et plausible
- [ ] Vérifier que la référence législative est renseignée et plausible
- [ ] Ne pas oublier de vérifier que le site web est fonctionnel en cliquant [ici](https://nom_de_la_branche--baremes-ipp.netlify.com/) et en incluant le nom de votre branche dans l'URL s'affichant sur le navigateur.

