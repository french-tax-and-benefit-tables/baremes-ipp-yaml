#! /usr/bin/env python


from setuptools import setup, find_packages


setup(
    name='OpenFisca-Baremes-IPP',
    version='0.1',
    author='Institut des politiques publiques',
    author_email='mahdi.benjelloul@gmail.com',
    url='https://framagit.org/french-tax-and-benefit-tables/baremes-ipp-yaml',
    include_package_data = True,  # Will read MANIFEST.in
    install_requires=[
        'OpenFisca-Core[web-api]',
        'importlib-metadata',
        ],
    extras_require = {},
    packages=find_packages(),
    test_suite='nose.collector',
    )
